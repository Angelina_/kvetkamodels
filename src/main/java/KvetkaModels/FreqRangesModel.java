package KvetkaModels;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class FreqRangesModel {

    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty() ;
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleDoubleProperty freqMin = new SimpleDoubleProperty();
    private SimpleDoubleProperty freqMax = new SimpleDoubleProperty();
    private SimpleStringProperty note = new SimpleStringProperty();

    public FreqRangesModel(){

    }

    public FreqRangesModel(boolean isCheck, int id, double freqMin, double freqMax, String note){
        this.isCheck = new SimpleBooleanProperty(isCheck); // new CheckBox();
        this.id = new SimpleIntegerProperty(id);
        this.freqMin = new SimpleDoubleProperty(freqMin);
        this.freqMax = new SimpleDoubleProperty(freqMax);
        this.note = new SimpleStringProperty(note);
    }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

    public double getFreqMin(){ return freqMin.get(); }
    public void setFreqMin(double value){ freqMin.set(value); }

    public double getFreqMax(){ return freqMax.get(); }
    public void setFreqMax(double value){ freqMax.set(value); }

    public String getNote(){ return note.get(); }
    public void setNote(String value){ note.set(value); }

}
