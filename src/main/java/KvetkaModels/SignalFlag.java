package KvetkaModels;

public enum SignalFlag {
    Unknown((byte) 0, "-"),
    Analog((byte) 1, "A"),
    Digital((byte) 2, "Ц");

    private byte type;
    private String name;

    SignalFlag(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getSignalFlag() {
        return type;
    }

    public String getSignalFlagName(){
        return name;
    }
}
