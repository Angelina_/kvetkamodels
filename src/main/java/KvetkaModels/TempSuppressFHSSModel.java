package KvetkaModels;

@InfoTable(name = NameTable.TempSuppressFHSS)
public class TempSuppressFHSSModel extends TempSuppressModel {

    public TempSuppressFHSSModel(){
        super();
    }

    public TempSuppressFHSSModel(int id, Led control, Led suppress, Led radiation, int stationId){
        super(id, control, suppress, radiation, stationId);
    }
}
