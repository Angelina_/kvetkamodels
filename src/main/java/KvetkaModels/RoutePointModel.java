package KvetkaModels;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

@InfoTable(name = NameTable.TableRoutePoint)
public class RoutePointModel extends AbstractCommonTable{
    
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private short numberPoint;
    private Coord coordinates = new Coord();
    private SimpleDoubleProperty segmentLength = new SimpleDoubleProperty();
    private SimpleIntegerProperty routeId = new SimpleIntegerProperty();

    public RoutePointModel(){
    }

    public RoutePointModel(int id, int routeId, short numberPoint, Coord coordinates, double segmentLength){
        this.id = new SimpleIntegerProperty(id);
        this.routeId = new SimpleIntegerProperty(routeId);
        this.numberPoint = numberPoint;
        this.coordinates = new Coord(coordinates);
        this.segmentLength = new SimpleDoubleProperty(segmentLength);
    }



    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

    public int getRouteId(){ return routeId.get(); }
    public void setRouteId(int value){ routeId.set(value); }

    public short getNumberPoint() { return numberPoint; }
    public void setNumberPoint(short value) { this.numberPoint = value; }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord value) { this.coordinates = value; }

    public double getSegmentLength() { return segmentLength.get(); }
    public SimpleDoubleProperty segmentLengthProperty() { return segmentLength; }
    public void setSegmentLength(double segmentLength) { this.segmentLength.set(segmentLength); }
    

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (RoutePointModel)record;
        
        numberPoint = newRec.numberPoint;
        coordinates.altitude = newRec.coordinates.altitude;
        coordinates.latitude = newRec.coordinates.latitude;
        coordinates.longitude = newRec.coordinates.longitude;
        segmentLength = newRec.segmentLength;
    }
}
