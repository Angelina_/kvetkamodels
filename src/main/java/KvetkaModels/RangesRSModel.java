package KvetkaModels;

@InfoTable(name = NameTable.TableFreqRangesJamming)
public class RangesRSModel extends SpecFreqsModel {

    public static String NameTable = "RangesRS";

    public RangesRSModel(){
        super();
    }

    public RangesRSModel(boolean isCheck, int id, double freqMin, double freqMax, String note, int stationId){
        super(isCheck, id, freqMin, freqMax, note, stationId);
    }
}
