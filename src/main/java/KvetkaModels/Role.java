package KvetkaModels;

public enum Role {

    Autonomic((byte) 0, "Автономная"),
    Master((byte) 1, "Ведущая"),
    Slave((byte) 2, "Ведомая");

    private byte type;
    private String name;

    Role(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getRoleType() {
        return type;
    }

    public String getRoleName(){
        return name;
    }

}
