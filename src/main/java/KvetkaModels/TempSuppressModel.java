package KvetkaModels;

import javafx.beans.property.SimpleIntegerProperty;

public class TempSuppressModel extends AbstractDependentASP{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private Led control;
    private Led suppress;
    private Led radiation;
    private SimpleIntegerProperty stationId = new SimpleIntegerProperty();

    public TempSuppressModel() { }

    public TempSuppressModel(int id, Led control, Led suppress, Led radiation, int stationId) {
        this.id = new SimpleIntegerProperty(id);
        this.control = control;
        this.suppress = suppress;
        this.radiation = radiation;
        this.stationId = new SimpleIntegerProperty(stationId);
    }

    public TempSuppressModel(TempSuppressModel tempSuppressModel) {
        this.id = tempSuppressModel.id;
        this.control = tempSuppressModel.control;
        this.suppress = tempSuppressModel.suppress;
        this.radiation = tempSuppressModel.radiation;
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public Led getControl() { return control; }
    public void setControl(Led control) { this.control = control; }

    public Led getSuppress() { return suppress; }
    public void setSuppress(Led suppress) { this.suppress = suppress; }

    public Led getRadiation() { return radiation; }
    public void setRadiation(Led radiation) { this.radiation = radiation; }

    public int getStationId(){ return stationId.get(); }
    public void setStationId(int value){ stationId.set(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (TempSuppressModel)record;

        control = newRec.control;
        radiation = newRec.radiation;
        suppress = newRec.suppress;
    }

    public TempSuppressFWSModel toTempSuppressFWSModel()
    {
        return new TempSuppressFWSModel(getId(), getControl(), getSuppress(), getRadiation(), getStationId());
    }

    public TempSuppressFHSSModel toTempSuppressFHSSModel()
    {
        return new TempSuppressFHSSModel(getId(), getControl(), getSuppress(), getRadiation(), getStationId());
    }
}
