package KvetkaModels;

public class Coord {

    public double latitude;
    public double longitude;
    public float altitude;

    public  Coord() { }

    public Coord(double latitude, double longitude, float altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public Coord(Coord coordinates) {
        this.latitude = coordinates.latitude;
        this.longitude = coordinates.longitude;
        this.altitude = coordinates.altitude;
    }

    public void update(Coord record)
    {
        altitude = record.altitude;
        latitude = record.latitude;
        longitude = record.longitude;
    }
}
