package KvetkaModels;

@InfoTable(name = NameTable.TempSuppressFF)
public class TempSuppressFWSModel extends TempSuppressModel {

    public TempSuppressFWSModel(){
        super();
    }

    public TempSuppressFWSModel(int id, Led control, Led suppress, Led radiation, int stationId){
        super(id, control, suppress, radiation, stationId);
    }
}
