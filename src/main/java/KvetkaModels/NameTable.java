package KvetkaModels;

public enum NameTable {
    TableJammer,
    TableFreqRangesElint,
    TableFreqRangesJamming,
    TableFreqKnown,
    TableFreqImportant,
    TableFreqForbidden,
    TableSectorsElint,
    TableADSB,
    TableResFF,
    TableTrackResFF,
    TableResFFJam,
    TableResFHSS,
    TableSubscriberResFHSS,
    TableTrackSubscriberResFHSS,
    TableResFHSSJam,
    TableGlobalProperties,
    TempSuppressFF,
    TempSuppressFHSS,
    TableDigitalResFF,
    TableAnalogResFF,
    TableResFFDistribution,
    TableRoute,
    TableRoutePoint,
    TableFHSSExcludedJam
}