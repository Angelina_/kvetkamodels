package KvetkaModels;

import javafx.beans.property.*;

@InfoTable(name = NameTable.TableResFHSSJam)
public class SuppressFHSSModel extends AbstractDependentASP{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private Byte sign;
    private SimpleDoubleProperty freqMin = new SimpleDoubleProperty();
    private SimpleDoubleProperty freqMax = new SimpleDoubleProperty();
    private InterferenceParams interferenceParams = new InterferenceParams();
    private SimpleIntegerProperty threshold = new SimpleIntegerProperty();
    private Byte[] letters = new Byte[]{};
    private Byte priority;
    private SimpleIntegerProperty stationId = new SimpleIntegerProperty();

    public SuppressFHSSModel(){

    }

    public SuppressFHSSModel(int id, boolean isCheck, byte sign, double freqMin, double freqMax, InterferenceParams interferenceParams, 
                             int threshold, Byte[] letters, byte priority, int stationId) {
        this.id = new SimpleIntegerProperty(id);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.sign = new Byte(sign);
        this.freqMin = new SimpleDoubleProperty(freqMin);
        this.freqMax = new SimpleDoubleProperty(freqMax);
        this.interferenceParams = new InterferenceParams(interferenceParams);
        this.threshold = new SimpleIntegerProperty(threshold);
        this.letters = letters;// new Byte[letters.length];
        this.priority = new Byte(priority);
        this.stationId = new SimpleIntegerProperty(stationId);
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public Byte getSign() { return sign; }
    public void setSign(Byte sign) { this.sign = sign; }

    public double getFreqMin() { return freqMin.get(); }
    public SimpleDoubleProperty freqMinProperty() { return freqMin; }
    public void setFreqMin(double freqMin) { this.freqMin.set(freqMin); }

    public double getFreqMax() { return freqMax.get(); }
    public SimpleDoubleProperty freqMaxProperty() { return freqMax; }
    public void setFreqMax(double freqMax) { this.freqMax.set(freqMax); }

    public InterferenceParams getInterferenceParams() { return interferenceParams; }
    public void setInterferenceParams(InterferenceParams interferenceParams) { this.interferenceParams = interferenceParams; }

    public int getThreshold() { return threshold.get(); }
    public SimpleIntegerProperty thresholdProperty() { return threshold; }
    public void setThreshold(int threshold) { this.threshold.set(threshold); }

    public void setLetters(Byte[] letters) { this.letters = letters; }
    public Byte[] getLetters() { return letters; }

    public Byte getPriority() { return priority; }
    public void setPriority(Byte priority) { this.priority = priority; }

    public int getStationId(){ return stationId.get(); }
    public void setStationId(int value){ stationId.set(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (SuppressFHSSModel)record;

        isCheck = newRec.isCheck;
        sign = newRec.sign;
        freqMin = newRec.freqMin;
        freqMax = newRec.freqMax;
        interferenceParams.update(newRec.interferenceParams);
        threshold = newRec.threshold;
        letters = newRec.letters;
        priority = newRec.priority;
    }
}
