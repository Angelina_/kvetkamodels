package KvetkaModels;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

@InfoTable(name = NameTable.TableSectorsElint)
public class SectorsModel extends AbstractDependentASP{

    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty() ;
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleDoubleProperty angleMin = new SimpleDoubleProperty();
    private SimpleDoubleProperty angleMax = new SimpleDoubleProperty();
    private SimpleStringProperty note = new SimpleStringProperty();
    private SimpleIntegerProperty stationId = new SimpleIntegerProperty();

    public SectorsModel(){

    }

    public SectorsModel(boolean isCheck, int id, double angleMin, double angleMax, String note, int stationId){
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.id = new SimpleIntegerProperty(id);
        this.angleMin = new SimpleDoubleProperty(angleMin);
        this.angleMax = new SimpleDoubleProperty(angleMax);
        this.note = new SimpleStringProperty(note);
        this.stationId = new SimpleIntegerProperty(stationId);
    }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

    public double getAngleMin(){ return angleMin.get(); }
    public void setAngleMin(double value){ angleMin.set(value); }

    public double getAngleMax(){ return angleMax.get(); }
    public void setAngleMax(double value){ angleMax.set(value); }

    public String getNote(){ return note.get(); }
    public void setNote(String value){ note.set(value); }

    public int getStationId(){ return stationId.get(); }
    public void setStationId(int value){ stationId.set(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (SectorsModel)record;

        angleMin = newRec.angleMin;
        angleMax = newRec.angleMax;
        note = newRec.note;
        isCheck = newRec.isCheck;
    }
}
