package KvetkaModels;

public enum Led {

    Empty ((byte) 0),
    Green ((byte) 1),
    Red ((byte) 2),
    Blue ((byte) 3),
    Yellow ((byte) 4),
    Gray ((byte)5);

    Led(byte b) {
    }

    public Byte getByte() {

        switch(this) {
            
            case Empty:
                return 0;

            case Green:
                return 1;

            case Red:
                return 2;

            case Blue:
                return 3;

            case Yellow:
                return 4;

            case Gray:
                return 5;

            default:
                break;
        }
        
        return 0;
    }

    public Led getLed(Byte b) {

        switch(b) {
            
            case 0:
                return Empty;

            case 1:
                return Green;

            case 2:
                return Red;

            case 3:
                return Blue;

            case 4:
                return Yellow;

            case 5:
                return Gray;

            default:
                break;
        }

        return Led.Empty;
    }

//
//
//    private Byte ledCode;
//
//    public void setLed(Byte ledCode) {
//        this.ledCode = ledCode;
//    }
//
//    Led(final Byte ledCode) {
//        this.ledCode = ledCode;
//    }
//
//    public Byte getLed() {
//        return ledCode;
//    }
}

