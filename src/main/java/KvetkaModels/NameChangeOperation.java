package KvetkaModels;

public enum NameChangeOperation {
    Add,
    Delete,
    Change
}
