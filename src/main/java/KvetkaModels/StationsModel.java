package KvetkaModels;

import javafx.beans.property.*;

@InfoTable(name = NameTable.TableJammer)
public class StationsModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
//    private SimpleDoubleProperty latitude = new SimpleDoubleProperty();
//    private SimpleDoubleProperty longitude = new SimpleDoubleProperty();
//    private SimpleFloatProperty altitude = new SimpleFloatProperty();
    private SimpleStringProperty callsign = new SimpleStringProperty();
    private Role role;
    private SimpleStringProperty note = new SimpleStringProperty();
    private Coord coordinates = new Coord();
    private SimpleBooleanProperty isOwn = new SimpleBooleanProperty();
    private SimpleIntegerProperty antennaAngle = new SimpleIntegerProperty();
    private CommunicationType communicationType;

    public StationsModel(){
    }

    public StationsModel(int id, Coord coordinates, String callsign, Role role, String note, boolean isOwn, int antennaAngle, CommunicationType communicationType){
        this.id = new SimpleIntegerProperty(id);
        this.coordinates = new Coord(coordinates);
        this.callsign = new SimpleStringProperty(callsign);
        this.role = role;
        this.note = new SimpleStringProperty(note);
        this.isOwn = new SimpleBooleanProperty(isOwn);
        this.antennaAngle = new SimpleIntegerProperty(antennaAngle);
        this.communicationType = communicationType;
    }

//    StationsModel(int id, double latitude, double longitude, float altitude, String callsign, int role, String note){
//        this.id = new SimpleIntegerProperty(id);
//        this.latitude = new SimpleDoubleProperty(latitude);
//        this.longitude = new SimpleDoubleProperty(longitude);
//        this.altitude = new SimpleFloatProperty(altitude);
//        this.callsign = new SimpleStringProperty(callsign);
//        this.role = new SimpleIntegerProperty(role);
//        this.note = new SimpleStringProperty(note);
//    }

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

//    public double getLatitude() { return latitude.get(); }
//    public void setLatitude(double value) { latitude.set(value); }
//
//    public double getLongitude() { return longitude.get(); }
//    public void setLongitude(double value) { this.longitude.set(value); }
//
//    public float getAltitude() { return altitude.get(); }
//    public void setAltitude(float value) { this.altitude.set(value); }

    public String getCallsign() { return callsign.get(); }
    public void setCallsign(String value) { this.callsign.set(value); }

    public Role getRole() { return role; }
    public void setRole(Role value) { this.role = value; }

    public String getNote(){ return note.get(); }
    public void setNote(String value){ note.set(value); }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord value) { this.coordinates = value; }

    public boolean getIsOwn(){ return isOwn.get(); }
    public void setIsOwn(boolean value){ isOwn.set(value); }

    public int getAntennaAngle(){ return antennaAngle.get(); }
    public void setAntennaAngle(int value){ antennaAngle.set(value); }

    public CommunicationType getCommunicationType(){ return communicationType; }
    public void setCommunicationType(CommunicationType value){ communicationType = value; }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (StationsModel)record;

        callsign = newRec.callsign;
        role = newRec.role;
        note = newRec.note;
        coordinates.altitude = newRec.coordinates.altitude;
        coordinates.latitude = newRec.coordinates.latitude;
        coordinates.longitude = newRec.coordinates.longitude;
        isOwn = newRec.isOwn;
        antennaAngle = newRec.antennaAngle;
        communicationType = newRec.communicationType;
    }

}
