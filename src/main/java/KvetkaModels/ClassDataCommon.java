package KvetkaModels;

import java.util.ArrayList;

import static java.util.stream.Collectors.toCollection;

public class ClassDataCommon {
    public ArrayList<AbstractCommonTable> listRecords;

    public ClassDataCommon()
    {
        listRecords = new ArrayList<AbstractCommonTable>();
    }

//    public <T extends Class<?>> ArrayList<T> ToList(Object obj, Class<T> clazz)
    public <T> ArrayList<T> toList(Object obj, Class<T> clazz)
    {
//        return (from t in ListRecords let c = t as T select c).ToList();
//        return ListRecords.forEach(t -> { c = t as T select c}).ToList();
//        return ListRecords.stream().flatMap(t-> Stream.of()).forEach(t -> t { c = t as T select c}).ToList();

//        ArrayList<T> li = new ArrayList<>();
//        T rec = (T)ListRecords.get(0);
       return listRecords.stream().map(t-> (T)t).collect(toCollection(ArrayList::new));
//        ListRecords.forEach(t -> {
//                    if (t instanceof T) {
//                        T c = (T) t;
//                        newList.add(c);
//                    }
//                }
//        )
    }

    public static <T> ClassDataCommon convertToListAbstractCommonTable(ArrayList<T> listRecords)
    {
        ClassDataCommon objListAbstractData = new ClassDataCommon();
        if (listRecords == null)
            return null;
        if (listRecords.size() == 0)
            return objListAbstractData;
        objListAbstractData.listRecords = listRecords.stream().map(t-> (AbstractCommonTable)t).collect(toCollection(ArrayList::new));
        return objListAbstractData;
    }
}
