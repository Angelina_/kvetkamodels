package KvetkaModels;

public enum ModeSound {
    Auto((byte) 0, "Автоматический"),
    Manual ((byte) 1, "Ручной");

    private byte mode;
    private String name;

    ModeSound(byte i, String n) {
        mode = i;
        name = n;
    }

    public byte getModeSound() {
        return mode;
    }

    public String getModeSoundName(){
        return name;
    }
}
