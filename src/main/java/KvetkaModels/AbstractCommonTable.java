package KvetkaModels;

public abstract  class AbstractCommonTable {

        public AbstractCommonTable(){}
        public AbstractCommonTable(int id)
        {
                this.id = id;
        }

        public int id;

        public abstract Object[] getKey();

        public abstract void update(AbstractCommonTable record);
}
