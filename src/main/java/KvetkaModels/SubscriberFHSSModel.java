package KvetkaModels;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.Date;

@InfoTable(name = NameTable.TableSubscriberResFHSS)

public class SubscriberFHSSModel extends AbstractCommonTable{
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleIntegerProperty fhssId = new SimpleIntegerProperty();
    private Date time = new Date();
    private TrackSubscriberFHSSModel position = new TrackSubscriberFHSSModel();

    public SubscriberFHSSModel(){}

    public SubscriberFHSSModel(int id, int fhssId, Date time, TrackSubscriberFHSSModel position){
        this.id = new SimpleIntegerProperty(id);
        this.fhssId = new SimpleIntegerProperty(fhssId);
        this.time = new Date(time.getTime());
        this.position = new TrackSubscriberFHSSModel(position);
    }

    public SubscriberFHSSModel(SubscriberFHSSModel subscriber){
        this.id = subscriber.id;
        this.fhssId = subscriber.fhssId;
        this.time = subscriber.time;
        this.position = subscriber.position;
    }

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }    
    
    public int getFhssId(){ return fhssId.get(); }
    public void setFhssId(int value){ fhssId.set(value); }
    
    public Date getTime() { return time; }
    public void setTime(Date value) { this.time = value; }

    public TrackSubscriberFHSSModel getPosition() { return position; }
    public void setPosition(TrackSubscriberFHSSModel value) { this.position = value; }


    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (SubscriberFHSSModel)record;

        time = newRec.time;
        position.update(newRec.position);
    }

}
