package KvetkaModels;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Date;
import java.util.List;

@InfoTable(name = NameTable.TableRoute)
public class RouteModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private short numberRoute;
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleDoubleProperty distance = new SimpleDoubleProperty();
    private Date time = new Date();
    private ObservableList<RoutePointModel> listPoints = FXCollections.observableArrayList(new RoutePointModel[]{});

    public RouteModel(){
    }

    public RouteModel(int id, short numberRoute, String name, double distance, Date time, List<RoutePointModel> listPoints){
        this.id = new SimpleIntegerProperty(id);
        this.numberRoute = numberRoute;
        this.name = new SimpleStringProperty(name);
        this.distance = new SimpleDoubleProperty(distance);
        this.time = new Date(time.getTime());
        this.listPoints = FXCollections.observableArrayList(listPoints);
    }

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }
    
    public short getNumberRoute() { return numberRoute; }
    public void setNumberRoute(short value) { this.numberRoute = value; }

    public String getName() { return name.get(); }
    public void setName(String value) { this.name.set(value); }
    
    public double getDistance() { return distance.get(); }
    public SimpleDoubleProperty distanceProperty() { return distance; }
    public void setDistance(double distance) { this.distance.set(distance); }
    
    public Date getTime() { return time; }
    public void setTime(Date time) { this.time = time; }
    
    public ObservableList<RoutePointModel> getListPoints() { return listPoints; }
    public void setListPoints(List<RoutePointModel> value) { this.listPoints = FXCollections.observableArrayList(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (RouteModel)record;

        numberRoute = newRec.numberRoute;
        name = newRec.name;
        distance = newRec.distance;
        time = new Date(newRec.time.getTime());
        listPoints = listPoints != null ? listPoints : newRec.listPoints;
        if (listPoints != newRec.listPoints)
        {
            listPoints.clear();
            listPoints.addAll(newRec.listPoints);
        }
    }
}
