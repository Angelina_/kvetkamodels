package KvetkaModels;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class FHSSExcludedModel extends AbstractDependentFHSS{
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
//    private SimpleIntegerProperty fhssId = new SimpleIntegerProperty();
    private SimpleDoubleProperty frequency = new SimpleDoubleProperty();
    private SimpleFloatProperty deviation = new SimpleFloatProperty();

    public FHSSExcludedModel(){
    }

    public FHSSExcludedModel(int id, double freq, float deviation, int fhssId) {
        this.id = new SimpleIntegerProperty(id);
        this.frequency = new SimpleDoubleProperty(freq);
        this.deviation = new SimpleFloatProperty(deviation);
        this.fhssId = new SimpleIntegerProperty(fhssId);;
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public double getFrequency() { return frequency.get(); }
    public SimpleDoubleProperty frequencyProperty() { return frequency; }
    public void setFrequency(double freqMin) { this.frequency.set(freqMin); }

    public float getDeviation() { return deviation.get(); }
    public void setDeviation(float value) { this.deviation.set(value);}

    public int getFhssId() { return fhssId.get(); }
    public void setFhssId(int value) { fhssId.set(value); }


    public SuppressFHSSExcludedModel toSuppressFhssExcluded()
    {
        return new SuppressFHSSExcludedModel(getId(), getFrequency(), getDeviation(), getFhssId());
    }
    

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (FHSSExcludedModel)record;

        frequency = newRec.frequency;
        deviation = newRec.deviation;
    }
}
