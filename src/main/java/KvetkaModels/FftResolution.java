package KvetkaModels;

public enum FftResolution {
    N32768((byte) 2, "32768"),
    N16384((byte) 3, "16384"),
    N8192((byte) 4, "8192"),
    N4096((byte) 5, "4096");

    private byte type;
    private String name;

    FftResolution(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getFftResolutionType() {
        return type;
    }

    public String getFftResolutionName(){
        return name;
    }

}
