package KvetkaModels;

import javafx.beans.property.*;

import java.util.Date;

@InfoTable(name = NameTable.TableResFFDistribution)
public class ReconFWSDistribModel extends AbstractCommonTable{
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleDoubleProperty frequency = new SimpleDoubleProperty();
    private SimpleFloatProperty band = new SimpleFloatProperty();
    private SimpleFloatProperty bearing = new SimpleFloatProperty();
    private SimpleFloatProperty bearingLinked = new SimpleFloatProperty();
    private SimpleIntegerProperty level = new SimpleIntegerProperty();
    private Coord coordinates = new Coord();
    private Date time = new Date();
    private SimpleFloatProperty distanceOwn = new SimpleFloatProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private SimpleBooleanProperty isRecording = new SimpleBooleanProperty();
    private SimpleStringProperty demodulation = new SimpleStringProperty();
    private SimpleDoubleProperty bandwidthKHz = new SimpleDoubleProperty();
    private SimpleIntegerProperty sampleRate = new SimpleIntegerProperty();
    private ModeSound mode;
    private SimpleDoubleProperty agcLevel = new SimpleDoubleProperty();
    private SimpleDoubleProperty mgcCoefficient = new SimpleDoubleProperty();
    public ReconFWSDistribModel() {
    }

    public ReconFWSDistribModel(int id, double frequency, float band, float bearing, float bearingLinked, int level,  Coord coordinates, Date time, float distanceOwn, boolean isCheck, boolean isRecording,
                                String demodulation, double bandwidthKHz, int sampleRate, ModeSound mode, double agcLevel, double mgcCoefficient) {
        this.id = new SimpleIntegerProperty(id);
        this.frequency = new SimpleDoubleProperty(frequency);
        this.band = new SimpleFloatProperty(band);
        this.bearing = new SimpleFloatProperty(bearing);
        this.bearingLinked = new SimpleFloatProperty(bearingLinked);
        this.level = new SimpleIntegerProperty(level);
        this.coordinates = new Coord(coordinates);
        this.time = new Date(time.getTime());
        this.distanceOwn = new SimpleFloatProperty(distanceOwn);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.isRecording = new SimpleBooleanProperty(isRecording);
        this.demodulation = new SimpleStringProperty(demodulation);
        this.bandwidthKHz = new SimpleDoubleProperty(bandwidthKHz);
        this.sampleRate = new SimpleIntegerProperty(sampleRate);
        this.mode = mode;
        this.agcLevel = new SimpleDoubleProperty(agcLevel);
        this.mgcCoefficient = new SimpleDoubleProperty(mgcCoefficient);
    }

    public ReconFWSDistribModel(ReconFWSDistribModel model) {
        this.id = model.id;
        this.frequency = model.frequency;
        this.band = model.band;
        this.bearing =model.bearing;
        this.bearingLinked = model.bearingLinked;
        this.level = model.level;
        this.coordinates = new Coord(model.coordinates);
        this.time = new Date(model.time.getTime());
        this.distanceOwn = model.distanceOwn;
        this.isCheck = model.isCheck;
        this.isRecording = model.isRecording;
        this.demodulation = model.demodulation;
        this.bandwidthKHz = model.bandwidthKHz;
        this.sampleRate = model.sampleRate;
        this.mode = model.mode;
        this.agcLevel = model.agcLevel;
        this.mgcCoefficient = model.mgcCoefficient;
    }

    public int getId() { return id.get(); }
    public void setId(int id) { this.id.set(id); }

    public double getFrequency() { return frequency.get(); }
    public SimpleDoubleProperty frequencyProperty() { return frequency; }
    public void setFrequency(double frequency) { this.frequency.set(frequency); }

    public float getBand() { return band.get(); }
    public SimpleFloatProperty bandProperty() { return band; }
    public void setBand(float band) { this.band.set(band); }

    public float getBearing() { return bearing.get(); }
    public SimpleFloatProperty bearingProperty() { return bearing; }
    public void setBearing(float bearing) { this.bearing.set(bearing); }

    public float getBearingLinked() { return bearingLinked.get(); }
    public SimpleFloatProperty bearingLinkedProperty() { return bearingLinked; }
    public void setBearingLinked(float bearing) { this.bearingLinked.set(bearing); }

    public int getLevel() { return level.get(); }
    public SimpleIntegerProperty levelProperty() { return level; }
    public void setLevel(int level) { this.level.set(level); }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord coordinates) { this.coordinates = coordinates; }

    public Date getTime() { return time; }
    public void setTime(Date time) { this.time = time; }

    public float getDistanceOwn() {return distanceOwn.get();}
    public SimpleFloatProperty distanceOwnProperty() {return distanceOwn;}
    public void setDistanceOwn(float distanceOwn) {this.distanceOwn.set(distanceOwn);}

    public boolean getIsCheck() {return isCheck.get();}
    public SimpleBooleanProperty isCheckProperty() {return isCheck;}
    public void setIsCheck(boolean isCheck) {this.isCheck.set(isCheck);}

    public boolean getIsRecording() {return isRecording.get();}
    public SimpleBooleanProperty isRecordingProperty() {return isRecording;}
    public void setIsRecording(boolean value) {this.isRecording.set(value);}

    public String getDemodulation(){ return demodulation.get(); }
    public void setDemodulation(String value){ demodulation.set(value); }

    public double getBandwidthKHz() { return bandwidthKHz.get(); }
    public SimpleDoubleProperty bandwidthKHzProperty() { return bandwidthKHz; }
    public void setBandwidthKHz(double bandwidthKHz) { this.bandwidthKHz.set(bandwidthKHz); }

    public int getSampleRate(){ return sampleRate.get(); }
    public void setSampleRate(int value){ sampleRate.set(value); }

    public ModeSound getMode(){ return mode; }
    public void setMode(ModeSound value){ mode =value; }

    public double getAgcLevel() { return agcLevel.get(); }
    public SimpleDoubleProperty agcLevelProperty() { return agcLevel; }
    public void setAgcLevel(double agcLevel) { this.agcLevel.set(agcLevel); }

    public double getMgcCoefficient() { return mgcCoefficient.get(); }
    public SimpleDoubleProperty mgcCoefficientProperty() { return mgcCoefficient; }
    public void setMgcCoefficient(double mgcCoefficient) { this.mgcCoefficient.set(mgcCoefficient); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (ReconFWSDistribModel)record;

        frequency = newRec.frequency;
        band = newRec.band;
        bearing = newRec.bearing;
        bearingLinked = newRec.bearingLinked;
        level = newRec.level;
        coordinates.update(newRec.coordinates);
        time = new Date(newRec.time.getTime());
        distanceOwn = newRec.distanceOwn;
        isCheck = newRec.isCheck;
        isRecording = newRec.isRecording;
        demodulation = newRec.demodulation;
        bandwidthKHz = newRec.bandwidthKHz;
        sampleRate = newRec.sampleRate;
        mode = newRec.mode;
        agcLevel = newRec.agcLevel;
        mgcCoefficient = newRec.mgcCoefficient;
    }
}
