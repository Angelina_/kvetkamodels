package KvetkaModels;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import java.util.Date;

@InfoTable(name = NameTable.TableResFF)
public class ReconFWSModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private Led control;
    private SignalFlag signalFlag;
    private SimpleDoubleProperty frequency = new SimpleDoubleProperty();
//    private SimpleFloatProperty band = new SimpleFloatProperty();
    private SimpleFloatProperty bearing1 = new SimpleFloatProperty();
    private SimpleFloatProperty bearing2 = new SimpleFloatProperty();
    private SimpleIntegerProperty level = new SimpleIntegerProperty();
//    private Byte type;
//    private Byte priority;
    private Coord coordinates = new Coord();
    private Date time = new Date();
//    private Byte radioNetwork;
    private SimpleFloatProperty distanceOwn = new SimpleFloatProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty() ;
    private Integer analogReconFwsId;
    private Integer digitalReconFwsId;
    private short quality;

    public ReconFWSModel() {
    }

    public ReconFWSModel(int id, Led control, SignalFlag signalFlag, double frequency, float bearing1, float bearing2, int level,  Coord coordinates, Date time, float distanceOwn, boolean isCheck, Integer analogReconFwsId, Integer digitalReconFwsId, short quality) {
        this.id = new SimpleIntegerProperty(id);
        this.control = control;
        this.signalFlag = signalFlag;
        this.frequency = new SimpleDoubleProperty(frequency);
//        this.band = new SimpleFloatProperty(band);
        this.bearing1 = new SimpleFloatProperty(bearing1);
        this.bearing2 = new SimpleFloatProperty(bearing2);
        this.level = new SimpleIntegerProperty(level);
//        this.type = type;
//        this.priority = priority;
        this.coordinates = new Coord(coordinates);
        this.time = new Date(time.getTime());
//        this.radioNetwork = radioNetwork;
        this.distanceOwn = new SimpleFloatProperty(distanceOwn);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.analogReconFwsId = analogReconFwsId;
        this.digitalReconFwsId = digitalReconFwsId;
        this.quality = quality;
    }
    
    public ReconFWSModel(ReconFWSModel model) {
        this.id = model.id;
        this.control = model.control;
        this.signalFlag = model.signalFlag;
        this.frequency = model.frequency;
        this.bearing1 =model.bearing1;
        this.bearing2 = model.bearing2;
        this.level = model.level;
        this.coordinates = new Coord(model.coordinates);
        this.time = new Date(model.time.getTime());
        this.distanceOwn = model.distanceOwn;
        this.isCheck = model.isCheck;
        this.analogReconFwsId = model.analogReconFwsId;
        this.digitalReconFwsId = model.digitalReconFwsId;
        this.quality = model.quality;
    }

    public int getId() { return id.get(); }
    public void setId(int id) { this.id.set(id); }

    public SignalFlag getSignalFlag() { return signalFlag; }
    public void setSignalFlag(SignalFlag signalFlag) { this.signalFlag = signalFlag; }

    public Led getControl() { return control; }
    public void setControl(Led control) { this.control = control; }

    public double getFrequency() { return frequency.get(); }
    public SimpleDoubleProperty frequencyProperty() { return frequency; }
    public void setFrequency(double frequency) { this.frequency.set(frequency); }

//    public float getBand() { return band.get(); }
//    public SimpleFloatProperty bandProperty() { return band; }
//    public void setBand(float band) { this.band.set(band); }

    public float getBearing1() { return bearing1.get(); }
    public SimpleFloatProperty bearing1Property() { return bearing1; }
    public void setBearing1(float bearing1) { this.bearing1.set(bearing1); }

    public float getBearing2() { return bearing2.get(); }
    public SimpleFloatProperty bearing2Property() { return bearing2; }
    public void setBearing2(float bearing2) { this.bearing2.set(bearing2); }

    public int getLevel() { return level.get(); }
    public SimpleIntegerProperty levelProperty() { return level; }
    public void setLevel(int level) { this.level.set(level); }

//    public Byte getType() { return type; }
//    public void setType(Byte type) { this.type = type; }
//
//    public Byte getPriority() { return priority; }
//    public void setPriority(Byte priority) { this.priority = priority; }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord coordinates) { this.coordinates = coordinates; }


    public Date getTime() { return time; }
    public void setTime(Date time) { this.time = time; }
//    public Byte getRadioNetwork() { return radioNetwork; }
//    public void setRadioNetwork(Byte radioNetwork) { this.radioNetwork = radioNetwork; }

    public float getDistanceOwn() {return distanceOwn.get();}
    public SimpleFloatProperty distanceOwnProperty() {return distanceOwn;}
    public void setDistanceOwn(float distanceOwn) {this.distanceOwn.set(distanceOwn);}
    
    public boolean isIsCheck() {return isCheck.get();}
    public SimpleBooleanProperty isCheckProperty() {return isCheck;}
    public void setIsCheck(boolean isCheck) {this.isCheck.set(isCheck);}

    public Integer getAnalogReconFwsId() { return analogReconFwsId; }
    public void setAnalogReconFwsId(Integer analogReconFwsId) { this.analogReconFwsId = analogReconFwsId; }

    public Integer getDigitalReconFwsId() { return digitalReconFwsId; }
    public void setDigitalReconFwsId(Integer digitalReconFwsId) { this.digitalReconFwsId = digitalReconFwsId; }

    public short getQuality() { return quality; }
    public void setQuality(short value) { this.quality = value; }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (ReconFWSModel)record;
        control = newRec.control;
        signalFlag = newRec.signalFlag;
        frequency = newRec.frequency;
//        band = newRec.band;
        bearing1 = newRec.bearing1;
        bearing2 = newRec.bearing2;
        level = newRec.level;
//        type = newRec.type;
//        priority = newRec.priority;
        coordinates.update(newRec.coordinates);
        time = new Date(newRec.time.getTime());
//        radioNetwork = newRec.radioNetwork;
        distanceOwn = newRec.distanceOwn;
        isCheck = newRec.isCheck;
//        digitalReconFwsId = newRec.digitalReconFwsId;
//        analogReconFwsId = newRec.analogReconFwsId;
        quality = newRec.quality;
    }
}
