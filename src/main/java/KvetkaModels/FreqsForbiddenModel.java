package KvetkaModels;

@InfoTable(name = NameTable.TableFreqForbidden)
public class FreqsForbiddenModel extends SpecFreqsModel {

    public static String NameTable = "FreqsForbidden";

    public FreqsForbiddenModel(){
        super();
    }

    public FreqsForbiddenModel(boolean isCheck, int id, double freqMin, double freqMax, String note, int stationId){
        super(isCheck, id, freqMin, freqMax, note, stationId);
    }
}
