package KvetkaModels;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SpecFreqsModel extends AbstractDependentASP{

    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty() ;
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleDoubleProperty freqMin = new SimpleDoubleProperty();
    private SimpleDoubleProperty freqMax = new SimpleDoubleProperty();
    private SimpleStringProperty note = new SimpleStringProperty();
    private SimpleIntegerProperty stationId = new SimpleIntegerProperty();

    public SpecFreqsModel(){

    }

    public SpecFreqsModel(boolean isCheck, int id, double freqMin, double freqMax, String note, int stationId){
        this.isCheck = new SimpleBooleanProperty(isCheck); // new CheckBox();
        this.id = new SimpleIntegerProperty(id);
        this.freqMin = new SimpleDoubleProperty(freqMin);
        this.freqMax = new SimpleDoubleProperty(freqMax);
        this.note = new SimpleStringProperty(note);
        this.stationId = new SimpleIntegerProperty(stationId);
    }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

    public double getFreqMin(){ return freqMin.get(); }
    public void setFreqMin(double value){ freqMin.set(value); }

    public double getFreqMax(){ return freqMax.get(); }
    public void setFreqMax(double value){ freqMax.set(value); }

    public String getNote(){ return note.get(); }
    public void setNote(String value){ note.set(value); }

    public int getStationId(){ return stationId.get(); }
    public void setStationId(int value){ stationId.set(value); }

    public RangesRIModel toRangesRIModel()
    {
        return new RangesRIModel(getIsCheck(), getId(), getFreqMin(), getFreqMax(), getNote(), getStationId());
    }

    public RangesRSModel toRangesRSModel()
    {
        return new RangesRSModel(getIsCheck(), getId(), getFreqMin(), getFreqMax(), getNote(), getStationId());
    }

    public FreqsKnownModel toFreqsKnownModel()
    {
        return new FreqsKnownModel(getIsCheck(), getId(), getFreqMin(), getFreqMax(), getNote(), getStationId());
    }

    public FreqsForbiddenModel toFreqsForbiddenModel()
    {
        return new FreqsForbiddenModel(getIsCheck(), getId(), getFreqMin(), getFreqMax(), getNote(), getStationId());
    }

    public FreqsImportantModel toFreqsImportantModel()
    {
        return new FreqsImportantModel(getIsCheck(), getId(), getFreqMin(), getFreqMax(), getNote(), getStationId());
    }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (SpecFreqsModel)record;

        freqMin = newRec.freqMin;
        freqMax = newRec.freqMax;
        note = newRec.note;
        isCheck = newRec.isCheck;
    }

}


