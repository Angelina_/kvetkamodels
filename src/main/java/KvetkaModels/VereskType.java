package KvetkaModels;

public enum VereskType {
    OPENED_SESSION((byte) 1, "ОТКРЫТ"),
    CLOSED_SESSION((byte) 2, "ЗАКРЫТ"),
    SIGNAL_DETECTED((byte) 3, "ОБНАРУЖЕН");

    private byte type;
    private String name;

    VereskType(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getVereskTypeType() {
        return type;
    }

    public String getVereskTypeName(){
        return name;
    }
}
