package KvetkaModels;

public enum PowerPercent {
    P100((byte) 0, "100"),
    P50((byte) 1, "50"),
    P25((byte) 2, "25");

    private byte type;
    private String name;

    PowerPercent(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getPowerPercentType() {
        return type;
    }

    public String getPowerPercentName(){
        return name;
    }
}
