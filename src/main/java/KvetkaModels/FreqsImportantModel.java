package KvetkaModels;

@InfoTable(name = NameTable.TableFreqImportant)
public class FreqsImportantModel extends SpecFreqsModel {

    public static String NameTable = "FreqsImportant";

    public FreqsImportantModel(){
        super();
    }

    public FreqsImportantModel(boolean isCheck, int id, double freqMin, double freqMax, String note, int stationId){
        super(isCheck, id, freqMin, freqMax, note, stationId);
    }
}
