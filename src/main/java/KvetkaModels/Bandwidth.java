package KvetkaModels;

public enum Bandwidth {
    B4000((int) 4000, "4000"),
    B8000((int) 8000, "8000"),
    B21000((int) 21000, "21000");

    private int type;
    private String name;

    Bandwidth(int i, String n) {
        type = i;
        name = n;
    }

    public int getBandwidth() {
        return type;
    }

    public String getBandwidthName(){
        return name;
    }
}
