package KvetkaModels;

import javafx.beans.property.*;

import java.util.Date;

@InfoTable(name = NameTable.TableDigitalResFF)
public class DigitalReconFWSModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private Date timeBegin = new Date();
    private Date timeEnd = new Date();
    private SimpleDoubleProperty frequency = new SimpleDoubleProperty();
    private DigitalSignalClass signalClass;
    private SimpleFloatProperty bearingOwn = new SimpleFloatProperty();
    private SimpleStringProperty subscriberInitiator = new SimpleStringProperty();
    private SimpleStringProperty subscriberRecipient = new SimpleStringProperty();
    private SimpleStringProperty radioNetwork = new SimpleStringProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty() ;
    private SimpleBooleanProperty hasMetadata = new SimpleBooleanProperty() ;
    private Coord coordinates = new Coord();
    private ReconFWSModel reconFWS;
    private VereskType vereskType;
    private SimpleLongProperty vereskDbSessionId = new SimpleLongProperty();


    public DigitalReconFWSModel() {
    }

    public DigitalReconFWSModel(int id, Date timeBegin, Date timeEnd, double frequency, DigitalSignalClass signalClass, float bearingOwn, String subscriberInitiator, String subscriberRecipient, String radioNetwork, boolean isCheck, boolean hasMetadata, Coord coordinates, ReconFWSModel reconFWS, VereskType vereskType, long vereskDbSessionId) {
        this.id = new SimpleIntegerProperty(id);
        this.timeBegin = new Date(timeBegin.getTime());
        this.timeEnd = new Date(timeEnd.getTime());
        this.frequency = new SimpleDoubleProperty(frequency);
        this.signalClass = signalClass;
        this.bearingOwn = new SimpleFloatProperty(bearingOwn);
        this.subscriberInitiator = new SimpleStringProperty(subscriberInitiator);
        this.subscriberRecipient = new SimpleStringProperty(subscriberRecipient);
        this.radioNetwork = new SimpleStringProperty(radioNetwork);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.hasMetadata = new SimpleBooleanProperty(hasMetadata);
        this.coordinates = new Coord(coordinates);
        this.reconFWS = reconFWS == null ? null : new ReconFWSModel(reconFWS);
        this.vereskType = vereskType;
        this.vereskDbSessionId = new SimpleLongProperty(vereskDbSessionId);
    }


    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (DigitalReconFWSModel)record;

        timeBegin = new Date(newRec.timeBegin.getTime());
        timeEnd = new Date(newRec.timeEnd.getTime());
        frequency = newRec.frequency;
        signalClass = newRec.signalClass;
        bearingOwn = newRec.bearingOwn;
        subscriberInitiator = newRec.subscriberInitiator;
        subscriberRecipient = newRec.subscriberRecipient;
        radioNetwork = newRec.radioNetwork;
        radioNetwork = newRec.radioNetwork;
        isCheck = newRec.isCheck;
        hasMetadata = newRec.hasMetadata;
        coordinates.update(newRec.coordinates);
        if (reconFWS == null)
        { reconFWS = newRec.reconFWS; }
        else
        { reconFWS.update(newRec.reconFWS); }
        vereskType = newRec.vereskType;
        vereskDbSessionId = newRec.vereskDbSessionId;
    }

    public int getId() { return id.get(); }
    public void setId(int id) { this.id.set(id); }

    public Date getTimeBegin() {return timeBegin;}
    public void setTimeBegin(Date timeBegin) {this.timeBegin = timeBegin;}

    public Date getTimeEnd() { return timeEnd;}
    public void setTimeEnd(Date timeEnd) {this.timeEnd = timeEnd;}

    public double getFrequency() { return frequency.get(); }
    public SimpleDoubleProperty frequencyProperty() { return frequency; }
    public void setFrequency(double frequency) { this.frequency.set(frequency); }

    public DigitalSignalClass getSignalClass() {return signalClass;}
    public void setSignalClass(DigitalSignalClass signalClass) {this.signalClass = signalClass;}

    public float getBearingOwn() { return bearingOwn.get(); }
    public SimpleFloatProperty bearingOwnProperty() { return bearingOwn; }
    public void setBearingOwn(float bearingOwn) { this.bearingOwn.set(bearingOwn); }

    public String getSubscriberInitiator(){ return subscriberInitiator.get(); }
    public void setSubscriberInitiator(String value){ subscriberInitiator.set(value); }

    public String getSubscriberRecipient(){ return subscriberRecipient.get(); }
    public void setSubscriberRecipient(String value){ subscriberRecipient.set(value); }

    public String getRadioNetwork(){ return radioNetwork.get(); }
    public void setRadioNetwork(String value){ radioNetwork.set(value); }

    public boolean getIsCheck() {return isCheck.get();}
    public SimpleBooleanProperty isCheckProperty() {return isCheck;}
    public void setIsCheck(boolean isCheck) {this.isCheck.set(isCheck);}

    public boolean getHasMetadata(){ return hasMetadata.get(); }
    public void setHasMetadata(boolean value){ hasMetadata.set(value); }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord coordinates) { this.coordinates = coordinates; }

    public ReconFWSModel getReconFWS() { return reconFWS;}
    public void setReconFWS(ReconFWSModel reconFWS) {this.reconFWS = reconFWS;}

    public VereskType getVereskType() {return vereskType;}
    public void setVereskType(VereskType vereskType) {this.vereskType = vereskType;}

    public long getVereskDbSessionId() { return vereskDbSessionId.get(); }
    public SimpleLongProperty vereskDbSessionIdProperty() { return vereskDbSessionId; }
    public void setVereskDbSessionId(long vereskDbSessionId) { this.vereskDbSessionId.set(vereskDbSessionId); }
}
