package KvetkaModels;

import javafx.beans.property.*;

@InfoTable(name = NameTable.TableGlobalProperties)
public class GlobalPropertiesModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    //GNSS
    private Coord gnss = new Coord();
    //General
    private SimpleIntegerProperty timeJamming = new SimpleIntegerProperty();
    //RI
    private SimpleIntegerProperty numberOfPhasesAveragings = new SimpleIntegerProperty();
    private SimpleIntegerProperty numberOfBearingAveragings = new SimpleIntegerProperty();
    private SimpleIntegerProperty courseAngle = new SimpleIntegerProperty();
    private SimpleIntegerProperty adaptiveThresholdFhss = new SimpleIntegerProperty();
    //FF
    private SimpleIntegerProperty numberOfResInLetter = new SimpleIntegerProperty();
    private SimpleIntegerProperty numberOfChannelsInLetter = new SimpleIntegerProperty();
    private SimpleIntegerProperty longWorkingSignalDurationMs= new SimpleIntegerProperty();
    private byte priority;
    private SimpleIntegerProperty threshold= new SimpleIntegerProperty();
    private SimpleIntegerProperty timeRadiationFF = new SimpleIntegerProperty();
    private SimpleBooleanProperty isTest = new SimpleBooleanProperty();
    //FHSS
    private FftResolution fhssFftResolution;
    private SimpleIntegerProperty timeRadiationFHSS = new SimpleIntegerProperty();
    //AFH
    private SimpleDoubleProperty searchSector = new SimpleDoubleProperty();
    private SimpleIntegerProperty searchTime = new SimpleIntegerProperty();
    //AM
    private SimpleIntegerProperty jsgAmPollInterval = new SimpleIntegerProperty();
    private PowerPercent jsgAmPower;


    public GlobalPropertiesModel(){
    }

    public GlobalPropertiesModel(int id, Coord gnss, int timeJamming,
                                 int numberOfPhasesAveragings, int numberOfBearingAveragings, int courseAngle, int adaptiveThresholdFhss,
                                 int numberOfResInLetter, int numberOfChannelsInLetter, int longWorkingSignalDurationMs,
                                 byte priority, int threshold, int timeRadiationFF, boolean isTest,
                                 FftResolution fhssFftResolution, int timeRadiationFHSS, 
                                 double searchSector, int searchTime, int jsgAmPollInterval, PowerPercent jsgAmPower){
        this.id = new SimpleIntegerProperty(id);
        this.gnss = new Coord(gnss);


        this.timeJamming = new SimpleIntegerProperty(timeJamming);
        
        this.numberOfPhasesAveragings = new SimpleIntegerProperty(numberOfPhasesAveragings);
        this.numberOfBearingAveragings = new SimpleIntegerProperty(numberOfBearingAveragings);
        this.courseAngle = new SimpleIntegerProperty(courseAngle);
        this.adaptiveThresholdFhss = new SimpleIntegerProperty(adaptiveThresholdFhss);
        
        this.numberOfResInLetter = new SimpleIntegerProperty(numberOfResInLetter);
        this.numberOfChannelsInLetter = new SimpleIntegerProperty(numberOfChannelsInLetter);
        this.longWorkingSignalDurationMs = new SimpleIntegerProperty(longWorkingSignalDurationMs);
        this.priority = priority;
        this.threshold = new SimpleIntegerProperty(threshold);
        this.timeRadiationFF = new SimpleIntegerProperty(timeRadiationFF);
        this.isTest = new SimpleBooleanProperty(isTest);

        this.fhssFftResolution = fhssFftResolution;
        this.timeRadiationFHSS = new SimpleIntegerProperty(timeRadiationFHSS);
        
        this.searchSector = new SimpleDoubleProperty(searchSector);
        this.searchTime = new SimpleIntegerProperty(searchTime);

        this.jsgAmPollInterval = new SimpleIntegerProperty(jsgAmPollInterval);
        this.jsgAmPower = jsgAmPower;
    }

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

    public Coord getGnss() { return gnss; }
    public void setGnss(Coord value) { this.gnss = value; }
    

    public int getTimeJamming(){ return timeJamming.get(); }
    public void setTimeJamming(int value){ timeJamming.set(value); }

    
    public int getNumberOfPhasesAveragings(){ return numberOfPhasesAveragings.get(); }
    public void setNumberOfPhasesAveragings(int value){ numberOfPhasesAveragings.set(value); }

    public int getNumberOfBearingAveragings(){ return numberOfBearingAveragings.get(); }
    public void setNumberOfBearingAveragings(int value){ numberOfBearingAveragings.set(value); }

    public int getCourseAngle(){ return courseAngle.get(); }
    public void setCourseAngle(int value){ courseAngle.set(value); }

    public int getAdaptiveThresholdFhss(){ return adaptiveThresholdFhss.get(); }
    public void setAdaptiveThresholdFhss(int value){ adaptiveThresholdFhss.set(value); }

    
    public int getNumberOfResInLetter() { return numberOfResInLetter.get(); }
    public void setNumberOfResInLetter(int value) { numberOfResInLetter.set(value); }

    public int getNumberOfChannelsInLetter() { return numberOfChannelsInLetter.get(); }
    public void setNumberOfChannelsInLetter(int value) { numberOfChannelsInLetter.set(value); }

    public int getLongWorkingSignalDurationMs() { return longWorkingSignalDurationMs.get(); }
    public void setLongWorkingSignalDurationMs(int value) { longWorkingSignalDurationMs.set(value); }

    public byte getPriority(){ return priority; }
    public void setPriority(byte value){ priority = value; }

    public int getThreshold() { return threshold.get(); }
    public void setThreshold(int value) { threshold.set(value); }

    public int getTimeRadiationFF() { return timeRadiationFF.get(); }
    public void setTimeRadiationFF(int value) { timeRadiationFF.set(value); }

    public boolean getIsTest() { return isTest.get(); }
    public void setIsTest(boolean value) { isTest.set(value); }

    
    public FftResolution getFhssFftResolution(){ return fhssFftResolution; }
    public void setFhssFftResolution(FftResolution value){ fhssFftResolution = value; }
    
    public int getTimeRadiationFHSS() { return timeRadiationFHSS.get(); }
    public void setTimeRadiationFHSS(int value) { timeRadiationFHSS.set(value); }

    
    public double getSearchSector() { return searchSector.get(); }
    public void setSearchSector(double value) { searchSector.set(value); }

    public int getSearchTime() { return searchTime.get(); }
    public void setSearchTime(int value) { searchTime.set(value); }


    public int getJsgAmPollInterval(){ return jsgAmPollInterval.get(); }
    public void setJsgAmPollInterval(int value){ jsgAmPollInterval.set(value); }
    
    public PowerPercent getJsgAmPower(){ return jsgAmPower; }
    public void setJsgAmPower(PowerPercent value){ jsgAmPower = value; }
    
    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (GlobalPropertiesModel)record;
        
        gnss.altitude = newRec.gnss.altitude;
        gnss.latitude = newRec.gnss.latitude;
        gnss.longitude = newRec.gnss.longitude;

        timeJamming = newRec.timeJamming;

        numberOfPhasesAveragings = newRec.numberOfPhasesAveragings;
        numberOfBearingAveragings = newRec.numberOfBearingAveragings;
        courseAngle = newRec.courseAngle;
        adaptiveThresholdFhss = newRec.adaptiveThresholdFhss;

        numberOfResInLetter = newRec.numberOfResInLetter;
        numberOfChannelsInLetter = newRec.numberOfChannelsInLetter;
        longWorkingSignalDurationMs = newRec.longWorkingSignalDurationMs;
        priority = newRec.priority;
        threshold = newRec.threshold;
        timeRadiationFF = newRec.timeRadiationFF;
        isTest = newRec.isTest;

        fhssFftResolution = newRec.fhssFftResolution;
        timeRadiationFHSS = newRec.timeRadiationFHSS;

        searchSector = newRec.searchSector;
        searchTime = newRec.searchTime;

        jsgAmPollInterval = newRec.jsgAmPollInterval;
        jsgAmPower = newRec.jsgAmPower;
    }
}
