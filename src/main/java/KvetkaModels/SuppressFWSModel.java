package KvetkaModels;

import javafx.beans.property.*;

@InfoTable(name = NameTable.TableResFFJam)
public class SuppressFWSModel extends AbstractDependentASP{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private Byte sign;
    private SimpleDoubleProperty frequency = new SimpleDoubleProperty();
    private SimpleFloatProperty bearing = new SimpleFloatProperty();
    private InterferenceParams interferenceParams = new InterferenceParams();
    private SimpleIntegerProperty threshold = new SimpleIntegerProperty();
    private Byte letter;
    private Byte priority;
    private Coord coordinates = new Coord();
    private SimpleIntegerProperty stationId = new SimpleIntegerProperty();

    public SuppressFWSModel(){

    }

    public SuppressFWSModel(int id, boolean isCheck, byte sign, double frequency, float bearing, InterferenceParams interferenceParams, 
                            Coord coordinates, int threshold, byte letter, byte priority, int stationId) {
        this.id = new SimpleIntegerProperty(id);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.sign = new Byte(sign);
        this.frequency = new SimpleDoubleProperty(frequency);
        this.bearing = new SimpleFloatProperty(bearing);
        this.interferenceParams = new InterferenceParams(interferenceParams);
        this.coordinates = new Coord(coordinates);
        this.threshold = new SimpleIntegerProperty(threshold);
        this.letter = new Byte(letter);
        this.priority = new Byte(priority);
        this.stationId = new SimpleIntegerProperty(stationId);
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public Byte getSign() { return sign; }
    public void setSign(Byte sign) { this.sign = sign; }

    public double getFrequency() { return frequency.get(); }
    public SimpleDoubleProperty frequencyProperty() { return frequency; }
    public void setFrequency(double frequency) { this.frequency.set(frequency); }

    public float getBearing() { return bearing.get(); }
    public SimpleFloatProperty bearingProperty() { return bearing; }
    public void setBearing(float bearing) { this.bearing.set(bearing); }

    public InterferenceParams getInterferenceParams() { return interferenceParams; }
    public void setInterferenceParams(InterferenceParams interferenceParams) { this.interferenceParams = interferenceParams; }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord value) { this.coordinates = value; }

    public int getThreshold() { return threshold.get(); }
    public SimpleIntegerProperty thresholdProperty() { return threshold; }
    public void setThreshold(int threshold) { this.threshold.set(threshold); }

    public Byte getLetter() { return letter; }
    public void setLetter(Byte letter) { this.letter = letter; }

    public Byte getPriority() { return priority; }
    public void setPriority(Byte priority) { this.priority = priority; }
    
    public int getStationId(){ return stationId.get(); }
    public void setStationId(int value){ stationId.set(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (SuppressFWSModel)record;

        isCheck = newRec.isCheck;
        sign = newRec.sign;
        frequency = newRec.frequency;
        bearing = newRec.bearing;
        interferenceParams.update(newRec.interferenceParams);
        coordinates.update(coordinates);
        threshold = newRec.threshold;
        letter = newRec.letter;
        priority = newRec.priority;
    }
}

