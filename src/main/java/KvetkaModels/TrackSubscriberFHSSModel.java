package KvetkaModels;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;

@InfoTable(name = NameTable.TableTrackSubscriberResFHSS)
public class TrackSubscriberFHSSModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleIntegerProperty subscriberFhssId = new SimpleIntegerProperty();
    private SimpleIntegerProperty fhssId = new SimpleIntegerProperty();
    private Coord coordinates = new Coord();
    private SimpleFloatProperty bearingOwn = new SimpleFloatProperty();
    private SimpleFloatProperty bearingLinked = new SimpleFloatProperty();
    private SimpleFloatProperty distanceOwn = new SimpleFloatProperty();
    private SimpleFloatProperty distanceLinked = new SimpleFloatProperty();

    public TrackSubscriberFHSSModel(){}

    public TrackSubscriberFHSSModel(int id, int subscriberFhssId, int fhssId, Coord coordinates, float bearingOwn, float bearingLinked, float distanceOwn, float distanceLinked){
        this.id = new SimpleIntegerProperty(id);
        this.subscriberFhssId = new SimpleIntegerProperty(subscriberFhssId);
        this.fhssId = new SimpleIntegerProperty(fhssId);
        this.coordinates = new Coord(coordinates);
        this.bearingOwn = new SimpleFloatProperty(bearingOwn);
        this.bearingLinked = new SimpleFloatProperty(bearingLinked);
        this.distanceOwn = new SimpleFloatProperty(distanceOwn);
        this.distanceLinked = new SimpleFloatProperty(distanceLinked);
    }

    public TrackSubscriberFHSSModel(TrackSubscriberFHSSModel track){
        this.id = track.id;
        this.subscriberFhssId = track.subscriberFhssId;
        this.fhssId =track.fhssId;
        this.coordinates = track.coordinates;
        this.bearingOwn = track.bearingOwn;
        this.bearingLinked = track.bearingLinked;
        this.distanceOwn = track.distanceOwn;
        this.distanceLinked = track.distanceLinked;
    }

    public int getId(){ return id.get(); }
    public void setId(int value){ id.set(value); }

    public int getSubscriberFhssId(){ return subscriberFhssId.get(); }
    public void setSubscriberFhssId(int value){ subscriberFhssId.set(value); }

    public int getFhssId(){ return fhssId.get(); }
    public void setFhssId(int value){ fhssId.set(value); }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord value) { this.coordinates = value; }

    public float getBearingOwn(){ return bearingOwn.get(); }
    public void setBearingOwn(float value){ bearingOwn.set(value); }

    public float getBearingLinked(){ return bearingLinked.get(); }
    public void setBearingLinked(float value){ bearingLinked.set(value); }

    public float getDistanceOwn(){ return distanceOwn.get(); }
    public void setDistanceOwn(float value){ distanceOwn.set(value); }

    public float getDistanceLinked(){ return distanceLinked.get(); }
    public void setDistanceLinked(float value){ distanceLinked.set(value); }

        

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (TrackSubscriberFHSSModel)record;

        coordinates.altitude = newRec.coordinates.altitude;
        coordinates.latitude = newRec.coordinates.latitude;
        coordinates.longitude = newRec.coordinates.longitude;
        bearingOwn = newRec.bearingOwn;
        bearingLinked = newRec.bearingLinked;
        distanceOwn = newRec.distanceOwn;
        distanceLinked = newRec.distanceLinked;
    }
}
