package KvetkaModels;

import java.util.ArrayList;

import static java.util.stream.Collectors.toCollection;

public class ClassDataDependASP {
    public ArrayList<AbstractDependentASP> listRecords;

    public ClassDataDependASP()
    {
        listRecords = new ArrayList<AbstractDependentASP>();
    }

    public <T> ArrayList<T> toList(Object obj, Class<T> clazz)
    {
        return listRecords.stream().map(t-> (T)t).collect(toCollection(ArrayList::new));
    }

    public static <T> ClassDataDependASP convertToDataDependASP(ArrayList<T> listRecords)
    {
        ClassDataDependASP objListAbstractData = new ClassDataDependASP();
        if (listRecords == null)
            return null;
        if (listRecords.size() == 0)
            return objListAbstractData;
        objListAbstractData.listRecords = listRecords.stream().map(t-> (AbstractDependentASP)t).collect(toCollection(ArrayList::new));
        return objListAbstractData;
    }
}
