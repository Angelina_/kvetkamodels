package KvetkaModels;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

@InfoTable(name = NameTable.TableResFHSS)
public class FHSSModel extends AbstractCommonTable{

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleDoubleProperty freqMin = new SimpleDoubleProperty();
    private SimpleDoubleProperty freqMax = new SimpleDoubleProperty();
    private SimpleFloatProperty band = new SimpleFloatProperty();
    private byte modulation;
    private short manipulationVelocity;
    private short stepKHz;
    private short impulseDuration;
    private ObservableList<SubscriberFHSSModel> listSubscribers = FXCollections.observableArrayList(new SubscriberFHSSModel[]{});
    private SimpleIntegerProperty idBearingDb = new SimpleIntegerProperty();

    public FHSSModel(){

    }

    public FHSSModel(int id, double freqMin, double freqMax, float band, byte modulation, short manipulationVelocity, short stepKHz, short impulseDuration, List<SubscriberFHSSModel> listSubscribers, int idBearingDb) {
        this.id = new SimpleIntegerProperty(id);
        this.freqMin = new SimpleDoubleProperty(freqMin);
        this.freqMax = new SimpleDoubleProperty(freqMax);
        this.band = new SimpleFloatProperty(band);
        this.modulation = modulation;
        this.manipulationVelocity = manipulationVelocity;
        this.stepKHz = stepKHz;
        this.impulseDuration = impulseDuration;
        this.listSubscribers = FXCollections.observableArrayList(listSubscribers);
        this.idBearingDb = new SimpleIntegerProperty(idBearingDb);
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public double getFreqMin() { return freqMin.get(); }
    public SimpleDoubleProperty freqMinProperty() { return freqMin; }
    public void setFreqMin(double freqMin) { this.freqMin.set(freqMin); }

    public double getFreqMax() { return freqMax.get(); }
    public SimpleDoubleProperty freqMaxProperty() { return freqMax; }
    public void setFreqMax(double freqMax) { this.freqMax.set(freqMax); }

    public float getBand() { return band.get(); }
    public void setBand(float value) { this.band.set(value);}

    public byte getModulation() { return modulation; }
    public void setModulation(byte value) { this.modulation = value; }

    public short getManipulationVelocity() { return manipulationVelocity; }
    public void setManipulationVelocity(short value) { this.manipulationVelocity = value; }
    
    public short getStepKHz() { return stepKHz; }
    public void setStepKHz(short value) { this.stepKHz = value; }
    
    public short getImpulseDuration() { return impulseDuration; }
    public void setImpulseDuration(short value) { this.impulseDuration = value; }

    public ObservableList<SubscriberFHSSModel> getListSubscribers() { return listSubscribers; }
    public void setListSubscribers(List<SubscriberFHSSModel> value) { this.listSubscribers = FXCollections.observableArrayList(value); }

    public int getIdBearingDb() { return idBearingDb.get(); }
    public void setIdBearingDb(int value) { idBearingDb.set(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (FHSSModel)record;
        
        freqMin = newRec.freqMin;
        freqMax = newRec.freqMax;
        band = newRec.band;
        modulation = newRec.modulation;
        manipulationVelocity = newRec.manipulationVelocity;
        stepKHz = newRec.stepKHz;
        impulseDuration = newRec.impulseDuration;
        idBearingDb = newRec.idBearingDb;
        listSubscribers = listSubscribers != null ? listSubscribers : newRec.listSubscribers;
        if (listSubscribers != newRec.listSubscribers)
        {
            listSubscribers.clear();
            listSubscribers.addAll(newRec.listSubscribers);
        }
    }
   
}
