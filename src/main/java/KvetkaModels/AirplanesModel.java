package KvetkaModels;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Date;

@InfoTable(name = NameTable.TableADSB)
public class AirplanesModel  extends AbstractCommonTable{

    private SimpleStringProperty icao = new SimpleStringProperty();
    private Coord coordinates = new Coord();
    private Date time = new Date();
    private Byte type;
    private SimpleStringProperty country = new SimpleStringProperty();
    private SimpleIntegerProperty id = new SimpleIntegerProperty();

    public AirplanesModel(){

    }

    public AirplanesModel(String icao, Coord coordinates, Date time, Byte type, String country){

        this.icao = new SimpleStringProperty(icao);
        this.coordinates = new Coord(coordinates);
        this.time = new Date(time.getTime());
        this.type= type;
        this.country = new SimpleStringProperty(country);
    }

    public AirplanesModel(String icao, Coord coordinates, Date time, Byte type, String country, int id){

        this.icao = new SimpleStringProperty(icao);
        this.coordinates = new Coord(coordinates);
        this.time = new Date(time.getTime());
        this.type= type;
        this.country = new SimpleStringProperty(country);
        this.id = new SimpleIntegerProperty(id);
    }

    public String getIcao() { return icao.get(); }
    public void setIcao(String value) { this.icao.set(value); }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord value) { this.coordinates = value; }

    public Date getTime() { return time; }
    public void setTime(Date value) { this.time = value; }

    public Byte getType() { return type; }
    public void setType(Byte value) { this.type = value; }

    public String getCountry() { return country.get(); }
    public void setCountry(String value) { this.country.set(value); }

    public int getId() { return id.get(); }
    public void setId(int value) {  this.id.set(value); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (AirplanesModel)record;

        icao = newRec.icao;
        time = newRec.time;
        type = newRec.type;
        country = newRec.country;
        coordinates.altitude = newRec.coordinates.altitude;
        coordinates.latitude = newRec.coordinates.latitude;
        coordinates.longitude = newRec.coordinates.longitude;
    }
}
