package KvetkaModels;

public class InterferenceParams {

    public byte modulation;
    public byte deviation;
    public byte manipulation;
    public byte duration;

    public InterferenceParams() { }

    public InterferenceParams(byte modulation, byte deviation, byte manipulation, byte duration) {
        this.modulation = modulation;
        this.deviation = deviation;
        this.manipulation = manipulation;
        this.duration = duration;
    }

    public InterferenceParams(InterferenceParams interferenceParams) {
        this.modulation = interferenceParams.modulation;
        this.deviation = interferenceParams.deviation;
        this.manipulation = interferenceParams.manipulation;
        this.duration = interferenceParams.duration;
    }

    public byte getModulation() { return modulation; }
    public void setModulation(byte modulation) { this.modulation = modulation; }

    public byte getDeviation() { return deviation; }
    public void setDeviation(byte deviation) { this.deviation = deviation; }

    public byte getManipulation() { return manipulation; }
    public void setManipulation(byte manipulation) { this.manipulation = manipulation; }

    public byte getDuration() { return duration; }
    public void setDuration(byte duration) { this.duration = duration; }

    
    public void update(InterferenceParams record) {
        modulation = record.modulation;
        deviation = record.deviation;
        manipulation = record.manipulation;
        duration = record.duration;
    }

}