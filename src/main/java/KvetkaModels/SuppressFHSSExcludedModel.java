package KvetkaModels;

@InfoTable(name = NameTable.TableFHSSExcludedJam)
public class SuppressFHSSExcludedModel extends FHSSExcludedModel {
    public static String NameTable = "SuppressFHSSExcludedModel";

    public SuppressFHSSExcludedModel(){
        super();
    }

    public SuppressFHSSExcludedModel(int id, double freq, float deviation, int fhssId){
        super(id, freq, deviation, fhssId);
    }
}
