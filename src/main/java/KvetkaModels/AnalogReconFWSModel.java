package KvetkaModels;

import javafx.beans.property.*;

import java.util.Date;

@InfoTable(name = NameTable.TableAnalogResFF)
public class AnalogReconFWSModel extends AbstractCommonTable{
    
    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleDoubleProperty frequency = new SimpleDoubleProperty();
    private SimpleFloatProperty band = new SimpleFloatProperty();
    private SimpleFloatProperty bearing1 = new SimpleFloatProperty();
    private SimpleIntegerProperty level = new SimpleIntegerProperty();
    private SimpleStringProperty typeSignal = new SimpleStringProperty();
    private SimpleIntegerProperty bandDemodulation = new SimpleIntegerProperty();
    private Coord coordinates = new Coord();
    private Date time = new Date();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private ReconFWSModel reconFWS;
    private SimpleDoubleProperty bandAudio = new SimpleDoubleProperty();


    public AnalogReconFWSModel() {
    }

    public AnalogReconFWSModel(int id, double frequency, float band, float bearing1, int level, String typeSignal, int bandDemodulation, Coord coordinates, Date time,  boolean isCheck, double bandAudio, ReconFWSModel reconFWS) {
        this.id = new SimpleIntegerProperty(id);
        this.frequency = new SimpleDoubleProperty(frequency);
        this.band = new SimpleFloatProperty(band);
        this.bearing1 = new SimpleFloatProperty(bearing1);
        this.level = new SimpleIntegerProperty(level);
        this.typeSignal = new SimpleStringProperty(typeSignal);
        this.bandDemodulation = new SimpleIntegerProperty(bandDemodulation);
        this.coordinates = new Coord(coordinates);
        this.time = new Date(time.getTime());
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.reconFWS = reconFWS == null ? null : new ReconFWSModel(reconFWS);
        this.bandAudio = new SimpleDoubleProperty(bandAudio);
    }

    public int getId() { return id.get(); }
    public void setId(int id) { this.id.set(id); }

    public double getFrequency() { return frequency.get(); }
    public SimpleDoubleProperty frequencyProperty() { return frequency; }
    public void setFrequency(double frequency) { this.frequency.set(frequency); }

    public float getBand() { return band.get(); }
    public SimpleFloatProperty bandProperty() { return band; }
    public void setBand(float band) { this.band.set(band); }

    public float getBearing1() { return bearing1.get(); }
    public SimpleFloatProperty bearing1Property() { return bearing1; }
    public void setBearing1(float bearing1) { this.bearing1.set(bearing1); }

    public int getLevel() { return level.get(); }
    public SimpleIntegerProperty levelProperty() { return level; }
    public void setLevel(int level) { this.level.set(level); }

    public String getTypeSignal() { return typeSignal.get(); }
    public void setTypeSignal(String typeSignal) { this.typeSignal.set(typeSignal); }

    public int getBandDemodulation() { return bandDemodulation.get(); }
    public void setBandDemodulation(int bandDemodulation) { this.bandDemodulation.set(bandDemodulation); }

    public Coord getCoordinates() { return coordinates; }
    public void setCoordinates(Coord coordinates) { this.coordinates = coordinates; }

    public Date getTime() { return time; }
    public void setTime(Date time) { this.time = time; }

    public boolean isIsCheck() {return isCheck.get();}
    public SimpleBooleanProperty isCheckProperty() {return isCheck;}
    public void setIsCheck(boolean isCheck) {this.isCheck.set(isCheck);}

    public ReconFWSModel getReconFWS() { return reconFWS;}
    public void setReconFWS(ReconFWSModel reconFWS) {this.reconFWS = reconFWS;}

    public double getBandAudio() { return bandAudio.get(); }
    public SimpleDoubleProperty bandAudioProperty() { return bandAudio; }
    public void setBandAudio(double bandAudio) { this.bandAudio.set(bandAudio); }

    @Override
    public Object[] getKey() {
        return new Object[] {getId()};
    }

    @Override
    public void update(AbstractCommonTable record) {
        var newRec = (AnalogReconFWSModel)record;

        frequency = newRec.frequency;
        band = newRec.band;
        bearing1 = newRec.bearing1;
        level = newRec.level;
        typeSignal = newRec.typeSignal;
        bandDemodulation = newRec.bandDemodulation;
        coordinates.update(newRec.coordinates);
        time = new Date(newRec.time.getTime());
        isCheck = newRec.isCheck;
        bandAudio = newRec.bandAudio;
        if (reconFWS == null)
        { reconFWS = newRec.reconFWS; }
        else
        { reconFWS.update(newRec.reconFWS); }
    }


}
