package KvetkaModels;

public enum NameTableOperation {
    Add,
    AddRange,
    RemoveRange,
    Change,
    ChangeRange,
    Delete,
    Clear,
    Load,
    LoadByFilterAsp,
    ClearByFilter,
    Update,
    None
}
