package KvetkaModels;

@InfoTable(name = NameTable.TableFreqKnown)
public class FreqsKnownModel extends SpecFreqsModel {

    public static String NameTable = "FreqsKnown";

    public FreqsKnownModel(){
        super();
    }

    public FreqsKnownModel(boolean isCheck, int id, double freqMin, double freqMax, String note, int stationId){
        super(isCheck, id, freqMin, freqMax, note, stationId);
    }
}
