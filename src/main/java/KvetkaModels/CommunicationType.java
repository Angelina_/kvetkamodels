package KvetkaModels;

//public enum CommunicationType {
//    RadioModem,
//    Modem4Wire,
//    Router3G_4G
//}

public enum CommunicationType {

    RadioModem((byte) 0, "Радиомодем"),
    Modem4Wire((byte) 1, "Модем 2, 4-проводный"),
    Router3G_4G((byte) 2, "3G/4G роутер");

    private byte type;
    private String name;

    CommunicationType(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getCommunicationType() {
        return type;
    }

    public String getCommunicationName(){
        return name;
    }

}
