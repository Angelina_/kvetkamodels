package KvetkaModels;

@InfoTable(name = NameTable.TableFreqRangesElint)
public class RangesRIModel extends SpecFreqsModel {

    public static String NameTable = "RangesRI";

    public RangesRIModel(){
        super();
    }

    public RangesRIModel(boolean isCheck, int id, double freqMin, double freqMax, String note, int stationId){
        super(isCheck, id, freqMin, freqMax, note, stationId);
    }
}
